/*
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   SLEPc - Scalable Library for Eigenvalue Problem Computations
   Copyright (c) 2002-2013, Universitat Politecnica de Valencia, Spain

   This file is part of SLEPc.

   SLEPc is free software: you can redistribute it and/or modify it under  the
   terms of version 3 of the GNU Lesser General Public License as published by
   the Free Software Foundation.

   SLEPc  is  distributed in the hope that it will be useful, but WITHOUT  ANY
   WARRANTY;  without even the implied warranty of MERCHANTABILITY or  FITNESS
   FOR  A  PARTICULAR PURPOSE. See the GNU Lesser General Public  License  for
   more details.

   You  should have received a copy of the GNU Lesser General  Public  License
   along with SLEPc. If not, see <http://www.gnu.org/licenses/>.
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*/

#if !defined(__SLEPCIP_H)
#define __SLEPCIP_H
#include <slepcvec.h>

PETSC_EXTERN PetscErrorCode IPInitializePackage(void);
/*S
    IP - Abstraction of a vector inner product, that can be defined
    in different ways. Using this object is not required for application
    programmers.

    Level: beginner

.seealso: IPCreate()
S*/
typedef struct _p_IP* IP;

/*J
    IPType - String with the name of the inner product. For complex scalars,
    it is possible to choose between a sesquilinear form (x,y)=x^H*M*y (the default)
    or a bilinear form (x,y)=x^T*M*y (without complex conjugation). In the case
    of real scalars, only the bilinear form (x,y)=x^T*M*y is available.
    Apart form these, there is also an indefinite inner product, defined by
    and indefinite matrix M.

    Level: advanced

.seealso: IPSetType(), IP
J*/
typedef const char* IPType;
#define IPBILINEAR     "bilinear"
#define IPSESQUILINEAR "sesquilinear"
#define IPINDEFINITE   "indefinite"

/* Logging support */
PETSC_EXTERN PetscClassId IP_CLASSID;

/*E
    IPOrthogType - Determines what type of orthogonalization to use

    Level: advanced

.seealso: IPSetOrthogonalization(), IPGetOrthogonalization(), IPOrthogonalize()
E*/
typedef enum { IP_ORTHOG_MGS,
               IP_ORTHOG_CGS } IPOrthogType;

/*E
    IPOrthogRefineType - Determines what type of refinement
    to use during orthogonalization

    Level: advanced

.seealso: IPSetOrthogonalization(), IPGetOrthogonalization(), IPOrthogonalize()
E*/
typedef enum { IP_ORTHOG_REFINE_NEVER,
               IP_ORTHOG_REFINE_IFNEEDED,
               IP_ORTHOG_REFINE_ALWAYS } IPOrthogRefineType;

PETSC_EXTERN PetscErrorCode IPCreate(MPI_Comm,IP*);
PETSC_EXTERN PetscErrorCode IPSetType(IP,IPType);
PETSC_EXTERN PetscErrorCode IPGetType(IP,IPType*);
PETSC_EXTERN PetscErrorCode IPSetOptionsPrefix(IP,const char *);
PETSC_EXTERN PetscErrorCode IPAppendOptionsPrefix(IP,const char *);
PETSC_EXTERN PetscErrorCode IPGetOptionsPrefix(IP,const char *[]);
PETSC_EXTERN PetscErrorCode IPSetFromOptions(IP);
PETSC_EXTERN PetscErrorCode IPSetOrthogonalization(IP,IPOrthogType,IPOrthogRefineType,PetscReal);
PETSC_EXTERN PetscErrorCode IPGetOrthogonalization(IP,IPOrthogType*,IPOrthogRefineType*,PetscReal*);
PETSC_EXTERN PetscErrorCode IPView(IP,PetscViewer);
PETSC_EXTERN PetscErrorCode IPDestroy(IP*);
PETSC_EXTERN PetscErrorCode IPReset(IP);

PETSC_EXTERN PetscErrorCode IPOrthogonalize(IP,PetscInt,Vec*,PetscInt,PetscBool*,Vec*,Vec,PetscScalar*,PetscReal*,PetscBool*);
PETSC_EXTERN PetscErrorCode IPBOrthogonalize(IP,PetscInt,Vec*,Vec*,PetscReal*,PetscInt,PetscBool*,Vec*,Vec*,PetscReal*,Vec,Vec,PetscScalar*,PetscReal*,PetscBool*);
PETSC_EXTERN PetscErrorCode IPBiOrthogonalize(IP,PetscInt,Vec*,Vec*,Vec,PetscScalar*,PetscReal*);
PETSC_EXTERN PetscErrorCode IPPseudoOrthogonalize(IP,PetscInt,Vec*,PetscReal*,Vec,PetscScalar*,PetscReal*,PetscBool*);
PETSC_EXTERN PetscErrorCode IPQRDecomposition(IP,Vec*,PetscInt,PetscInt,PetscScalar*,PetscInt);

PETSC_EXTERN PetscErrorCode IPSetMatrix(IP,Mat);
PETSC_EXTERN PetscErrorCode IPGetMatrix(IP,Mat*);
PETSC_EXTERN PetscErrorCode IPApplyMatrix(IP,Vec,Vec);

PETSC_EXTERN PetscErrorCode IPInnerProduct(IP,Vec,Vec,PetscScalar*);
PETSC_EXTERN PetscErrorCode IPInnerProductBegin(IP,Vec,Vec,PetscScalar*);
PETSC_EXTERN PetscErrorCode IPInnerProductEnd(IP,Vec,Vec,PetscScalar*);
PETSC_EXTERN PetscErrorCode IPMInnerProduct(IP,Vec,PetscInt,const Vec[],PetscScalar*);
PETSC_EXTERN PetscErrorCode IPMInnerProductBegin(IP,Vec,PetscInt,const Vec[],PetscScalar*);
PETSC_EXTERN PetscErrorCode IPMInnerProductEnd(IP,Vec,PetscInt,const Vec[],PetscScalar*);
PETSC_EXTERN PetscErrorCode IPNorm(IP,Vec,PetscReal*);
PETSC_EXTERN PetscErrorCode IPNormBegin(IP,Vec,PetscReal*);
PETSC_EXTERN PetscErrorCode IPNormEnd(IP,Vec,PetscReal*);

PETSC_EXTERN PetscFunctionList IPList;
PETSC_EXTERN PetscBool         IPRegisterAllCalled;
PETSC_EXTERN PetscErrorCode IPRegisterAll(void);
PETSC_EXTERN PetscErrorCode IPRegister(const char[],PetscErrorCode(*)(IP));

PETSC_EXTERN PetscErrorCode IPGetOperationCounters(IP,PetscInt*);
PETSC_EXTERN PetscErrorCode IPResetOperationCounters(IP);

#endif
