%-------------------------------------------------------
% SLEPc Users Manual
%-------------------------------------------------------
\chapter{\label{cap:qep}QEP: Quadratic Eigenvalue Problems}
%-------------------------------------------------------

\noindent The Quadratic Eigenvalue Problem (\ident{QEP}) solver object is intended for addressing polynomial eigenproblems of degree 2. Apart from specific quadratic solvers, it provides the possibility of reducing the problem to a generalized eigenvalue problem via linearization, then solving it with the \ident{EPS} package.

As in the case of \ident{SVD}, the user interface of \ident{QEP} is very similar to \ident{EPS}. We highlight the main differences both in usage and theoretical properties.

\section{\label{sec:qep}Overview of Quadratic Eigenproblems}

In this section, we review some basic properties of quadratic eigenvalue problems. The main goal is to set up the notation as well as to describe the linearization approaches that will be employed for solving via the \ident{EPS} object. For additional background material about the quadratic eigenproblem, the reader is referred to \citep{Tisseur:2001:QEP}. As always, some details of the implemented methods can be found in the \slepc \hyperlink{str}{technical reports}.

In many applications, e.g., problems arising from second-order differential equations such as the analysis of damped vibrating systems, the eigenproblem to be solved is quadratic,
\begin{equation}
(\lambda^2M+\lambda C+K)x=0,\label{eq:eigquad}
\end{equation}
where $M,C,K\in\mathbb{C}^{n\times n}$ are the coefficients of a matrix polynomial of degree 2, $\lambda\in\mathbb{C}$ is the eigenvalue and $x\in\mathbb{C}^n$ is the eigenvector. As in the case of linear eigenproblems, the eigenvalues and eigenvectors can be complex even in the case that all three matrices are real.

It is important to point out some outstanding differences with respect to the linear eigenproblem. In the quadratic eigenproblem, the number of eigenvalues is $2n$, and the corresponding eigenvectors do not form a linearly independent set. If $M$ is singular, some eigenvalues are infinite. Even when the three matrices are symmetric and positive definite, there is no guarantee that the eigenvalues are real, but still methods can exploit symmetry to some extent. Furthermore, numerical difficulties are more likely than in the linear case, so the computed solution can sometimes be untrustworthy.

If Eq.\ \ref{eq:eigquad} is written as $Q(\lambda)x=0$, where $Q$ is the matrix polynomial, then multiplication by $\lambda^{-2}$ results in $R(\lambda^{-1})x=0$, where $R$ is a matrix polynomial with the coefficients in the reverse order. In other words, if a method is available for computing the largest eigenvalues, then reversing the roles of $M$ and $K$ results in the computation of the smallest eigenvalues. In general, it is also possible to formulate different spectral transformations for computing eigenvalues closest to a given target.

\paragraph{Problem Types.}

As in the case of linear eigenproblems, there are some particular properties of the coefficient matrices that confer a certain structure to the quadratic eigenproblem, e.g., symmetry of the spectrum with respect to the real or imaginary axes. These structures are important as long as the solvers are able to exploit them.

\begin{itemize}
\item Hermitian (symmetric) problems, when $M$, $C$, $K$ are all Hermitian (symmetric). Eigenvalues are real or come in complex conjugate pairs. Furthermore, if $M>0$ and $C,K\geq 0$ then the system is stable, i.e., $\text{Re}(\lambda)\leq 0$.
\item Hyperbolic problems, a particular class of Hermitian problems where $M>0$ and $(x^*Cx)^2>4(x^*Mx)(x^*Kx)$ for all nonzero $x\in\mathbb{C}^n$. All eigenvalues are real, and form two separate groups of $n$ eigenvalues, each of them having linearly independent eigenvectors.
\item Overdamped problems, a specialized type of hyperbolic problems, where $C>0$ and $K\geq 0$. The eigenvalues are non-positive.
\item Gyroscopic problems, when $M$, $K$ are Hermitian, $M>0$, and $C$ is skew-Hermitian, $C=-C^*$. The spectrum is symmetric with respect to the imaginary axis, and in the real case, it has a Hamiltonian structure, i.e., eigenvalues come in quadruples $(\lambda,\bar{\lambda},-\lambda,-\bar{\lambda})$.
\end{itemize}

\paragraph{Equivalent Eigenvalue Problems.}

It is possible to transform the quadratic eigenvalue problem to a linear generalized eigenproblem $Az=\lambda Bz$ (linearization) by doubling the order of the system, i.e., $A,B\in\mathbb{C}^{2n\times 2n}$. There are many ways of doing this, and all of them are based on defining the eigenvector of the linear problem as 
\begin{equation}
\label{eq:linevec}
z=\left[\begin{array}{c}x\\\lambda x\end{array}\right],
\end{equation}
or a similar way. Below, we show some of the most common linearizations.

\begin{itemize}
\item Non-symmetric linearizations. The resulting matrix pencil has no particular structure.
\begin{equation}
\label{eq:n1}
\mbox{N1:}\qquad
\left[\begin{array}{cc}0 & I\\-K & -C\end{array}\right]-\lambda\left[\begin{array}{cc}I & 0\\0 & M\end{array}\right]
\end{equation}
\begin{equation}
\label{eq:n2}
\mbox{N2:}\qquad
\left[\begin{array}{cc}-K & 0\\0 & I\end{array}\right]-\lambda\left[\begin{array}{cc}C & M\\I & 0\end{array}\right]
\end{equation}

\medskip
\item Symmetric linearizations. If $M$, $C$, and $K$ are all symmetric (Hermitian), the resulting matrix pencil is symmetric (Hermitian), although indefinite.
\begin{equation}
\label{eq:s1}
\mbox{S1:}\qquad
\left[\begin{array}{cc}0 & -K\\-K & -C\end{array}\right]-\lambda\left[\begin{array}{cc}-K & 0\\0 & M\end{array}\right]
\end{equation}
\begin{equation}
\label{eq:s2}
\mbox{S2:}\qquad
\left[\begin{array}{cc}-K & 0\\0 & M\end{array}\right]-\lambda\left[\begin{array}{cc}C & M\\ M & 0\end{array}\right]
\end{equation}

\medskip
\item Hamiltonian linearizations. If the quadratic eigenproblem is gyroscopic, one of the matrices is Hamiltonian and the other is skew-Hamiltonian. The first form (H1) is recommended when $M$ is singular, whereas the second form (H2) is recommended when $K$ is singular.
\begin{equation}
\label{eq:h1}
\mbox{H1:}\qquad
\left[\begin{array}{cc}K & 0\\C & K\end{array}\right]-\lambda\left[\begin{array}{cc} 0 & K\\-M & 0\end{array}\right]
\end{equation}
\begin{equation}
\label{eq:h2}
\mbox{H2:}\qquad
\left[\begin{array}{cc}0 & -K\\M & 0\end{array}\right]-\lambda\left[\begin{array}{cc}M & C\\ 0 & M\end{array}\right]
\end{equation}
\end{itemize}

In \slepc, some solvers of the \ident{QEP} class are based on using one of the above linearizations for solving the quadratic eigenproblem. These solvers make use of linear eigensolvers from the \ident{EPS} package.

We could also consider the \emph{reversed} forms, e.g., the reversed form of N2 is
\begin{equation}
\label{eq:n2r}
\mbox{N2-R:}\qquad
\left[\begin{array}{cc}-C & -M\\I & 0\end{array}\right]-\frac{1}{\lambda}\left[\begin{array}{cc}K & 0\\0 & I\end{array}\right],
\end{equation}
which is equivalent to the form N1 for the problem $R(\lambda^{-1})x=0$. These reversed forms are not implemented in \slepc, but the user can use them simply by reversing the roles of $M$ and $K$, and considering the reciprocals of the computed eigenvalues.

%---------------------------------------------------
\section{Basic Usage}

The user interface of the \ident{QEP} package is very similar to \ident{EPS}. For basic usage, the only noteworthy difference is that three matrices have to be supplied.

A basic example code for solving a quadratic eigenproblem with \ident{QEP} is shown in Figure \ref{fig:ex-qep}. The required steps are the same as those described in chapter \ref{cap:eps} for the linear eigenproblem. As always, the solver context is created with \ident{QEPCreate}. The three problem matrices are provided with \ident{QEPSetOperators}, and the problem type is specified with \ident{QEPSetProblemType}. Calling \ident{QEPSetFromOptions} allows the user to set up various options through the command line. The call to \ident{QEPSolve} invokes the actual solver. Then, the solution is retrieved with \ident{QEPGetConverged} and \ident{QEPGetEigenpair}. Finally, \ident{QEPDestroy} destroys the object.

\begin{figure}
\begin{Verbatim}[fontsize=\small,numbers=left,numbersep=6pt,xleftmargin=15mm]
QEP         qep;       /*  eigensolver context  */
Mat         M, C, K;   /*  matrices of the QEP  */
Vec         xr, xi;    /*  eigenvector, x       */
PetscScalar kr, ki;    /*  eigenvalue, k        */
PetscInt    j, nconv;
PetscReal   error;

QEPCreate( PETSC_COMM_WORLD, &qep );
QEPSetOperators( qep, M, C, K );
QEPSetProblemType( qep, QEP_GENERAL );
QEPSetFromOptions( qep );
QEPSolve( qep );
QEPGetConverged( qep, &nconv );
for (j=0; j<nconv; j++) {
  QEPGetEigenpair( qep, j, &kr, &ki, xr, xi );
  QEPComputeRelativeError( qep, j, &error );
}
QEPDestroy( qep );
\end{Verbatim}
\caption{\label{fig:ex-qep}Example code for basic solution with \ident{QEP}.}
\end{figure}


%---------------------------------------------------
\section{Defining the Problem}

As mentioned in \S\ref{sec:qep}, it is possible to distinguish among different problem types. The problem types currently supported for \ident{QEP} are listed in Table \ref{tab:ptypeq}. The goal when choosing an appropriate problem type is to let the solver exploit the underlying structure, in order to possibly compute the solution more accurately with less floating-point operations. When in doubt, use the default problem type (\texttt{QEP\_GENERAL}). 

\begin{table}[b]
\centering
{\small \begin{tabular}{lll}
Problem Type  & \ident{QEPProblemType}    & Command line key\\\hline
General       & \texttt{QEP\_GENERAL}     & \texttt{-qep\_general}\\
Hermitian     & \texttt{QEP\_HERMITIAN}   & \texttt{-qep\_hermitian}\\
Gyroscopic    & \texttt{QEP\_GYROSCOPIC}  & \texttt{-qep\_gyroscopic}\\\hline
\end{tabular} }
\caption{\label{tab:ptypeq}Problem types considered in \ident{QEP}.}
\end{table}

The problem type can be specified at run time with the corresponding command line key or, more usually, within the program with the function
	\findex{QEPSetProblemType}
	\begin{Verbatim}[fontsize=\small]
	EPSSetProblemType(QEP eps,QEPProblemType type);
	\end{Verbatim}

Apart from the problem type, the definition of the problem is completed with the number and location of the eigenvalues to compute. This is done very much like in \ident{EPS}, but with minor differences.

	The number of eigenvalues (and eigenvectors) to compute, \texttt{nev}, is specified with the function%
	\findex{QEPSetDimensions}
	\begin{Verbatim}[fontsize=\small]
	QEPSetDimensions(QEP qep,PetscInt nev,PetscInt ncv,PetscInt mpd);
	\end{Verbatim}
The default is to compute only one. This function also allows control over the dimension of the subspaces used internally. The second argument, \texttt{ncv}, is the number of column vectors to be used by the solution algorithm, that is, the largest dimension of the working subspace. The third argument, \texttt{mpd}, is the maximum projected dimension. These parameters can also be set from the command line with \Verb!-qep_nev!, \Verb!-qep_ncv! and \Verb!-qep_mpd!.

\begin{table}
\centering
{\small \begin{tabular}{lll}
\texttt{QEPWhich}                  & Command line key                   & Sorting criterion \\\hline
\texttt{QEP\_LARGEST\_MAGNITUDE}   & \texttt{-qep\_largest\_magnitude}  & Largest $|\lambda|$ \\
\texttt{QEP\_SMALLEST\_MAGNITUDE}  & \texttt{-qep\_smallest\_magnitude} & Smallest $|\lambda|$ \\
\texttt{QEP\_LARGEST\_REAL}        & \texttt{-qep\_largest\_real}       & Largest $\mathrm{Re}(\lambda)$ \\
\texttt{QEP\_SMALLEST\_REAL}       & \texttt{-qep\_smallest\_real}      & Smallest $\mathrm{Re}(\lambda)$ \\
\texttt{QEP\_LARGEST\_IMAGINARY}   & \texttt{-qep\_largest\_imaginary}  & Largest $\mathrm{Im}(\lambda)$\footnotemark \\
\texttt{QEP\_SMALLEST\_IMAGINARY}  & \texttt{-qep\_smallest\_imaginary} & Smallest $\mathrm{Im}(\lambda)$\addtocounter{footnote}{-1}\footnotemark \\\hline
\texttt{QEP\_TARGET\_MAGNITUDE}    & \texttt{-qep\_target\_magnitude}   & Smallest $|\lambda-\tau|$ \\
\texttt{QEP\_TARGET\_REAL}         & \texttt{-qep\_target\_real}        & Smallest $|\mathrm{Re}(\lambda-\tau)|$ \\
\texttt{QEP\_TARGET\_IMAGINARY}    & \texttt{-qep\_target\_imaginary}   & Smallest $|\mathrm{Im}(\lambda-\tau)|$ \\\hline
\end{tabular} }
\caption{\label{tab:portionq}Available possibilities for selection of the eigenvalues of interest in \ident{QEP}.}
\end{table}

\footnotetext{If \slepc is compiled for real scalars, then the absolute value of the imaginary part, $|\mathrm{Im}(\lambda)|$, is used for eigenvalue selection and sorting.}

	For the selection of the portion of the spectrum of interest, there are several alternatives listed in Table~\ref{tab:portionq}, to be selected with the function
	\findex{QEPSetWhichEigenpairs}
	\begin{Verbatim}[fontsize=\small]
	QEPSetWhichEigenpairs(QEP qep,QEPWhich which);
	\end{Verbatim}
The default is to compute the largest magnitude eigenvalues.
For the sorting criteria relative to a target value, $\tau$ must be specified with:
	\findex{QEPSetTarget}
	\begin{Verbatim}[fontsize=\small]
	QEPSetTarget(QEP qep,PetscScalar target);
	\end{Verbatim}
or in the command-line with \Verb!-qep_target!. As in \ident{EPS}, complex values of $\tau$ are allowed only in complex scalar SLEPc builds. The criteria relative to a target must be used in combination with a spectral transformation as explained in \S\ref{sec:qst}.

%---------------------------------------------------
\section{Selecting the Solver}

The solution method can be specified procedurally with
	\findex{QEPSetType}
	\begin{Verbatim}[fontsize=\small]
	QEPSetType(QEP qep,QEPType method);
	\end{Verbatim}
or via the options database command \Verb!-qep_type! followed by the name of the method. The methods currently available in \ident{QEP} are listed in Table~\ref{tab:solversq}. The first one is based on linearization, while the rest either work implicitly on a linearization, or operate directly on the quadratic problem without linearizing.

\begin{table}
\centering
{\small \begin{tabular}{lll}
                   &                      & {\footnotesize Options} \\
Method             & \ident{QEPType}      & {\footnotesize Database Name}\\\hline
Linearization      & \texttt{QEPLINEAR}   & \texttt{linear} \\
Quadratic Arnoldi  & \texttt{QEPQARNOLDI} & \texttt{qarnoldi} \\
Quadratic Lanczos  & \texttt{QEPQLANCZOS} & \texttt{qlanczos} \\\hline
\end{tabular} }
\caption{\label{tab:solversq}Quadratic eigenvalue solvers available in the \ident{QEP} module.}
\end{table}

The \texttt{QEPLINEAR} method carries out a linearization of the quadratic eigenproblem, as described in \S\ref{sec:qep}, resulting in a generalized eigenvalue problem that is handled by an \ident{EPS} object created internally. If required, this \ident{EPS} object can be extracted with the operation
	\findex{QEPLinearGetEPS}
	\begin{Verbatim}[fontsize=\small]
	QEPLinearGetEPS(QEP qep,EPS *eps);
	\end{Verbatim}
This allows the application programmer to set any of the \ident{EPS} options directly within the code. Also, it is possible to change the \ident{EPS} options through the command-line, simply by prefixing the \ident{EPS} options with \texttt{-qep\_}.

The expression used in the linearization is specified by two parameters:
\begin{enumerate}
\item The problem type set with \ident{QEPProblemType}, which chooses from non-symmetric, symmetric, and Hamiltonian linearizations.
\item The companion form, 1 or 2, that can be chosen with
	\findex{QEPLinearSetCompanionForm}
	\begin{Verbatim}[fontsize=\small]
   QEPLinearSetCompanionForm(QEP qep,PetscInt cform);
	\end{Verbatim}
\end{enumerate}

Another option of the \texttt{QEPLINEAR} solver is whether the matrices of the linearized problem are created explicitly or not. This is set with the function
	\findex{QEPLinearSetExplicitMatrix}
	\begin{Verbatim}[fontsize=\small]
	QEPLinearSetExplicitMatrix(QEP qep,PetscBool exp);
	\end{Verbatim}
In the case of explicit creation, matrices $A$ and $B$ are created as real \texttt{Mat}'s, with explicit storage, whereas the implicit option works with \emph{shell} \texttt{Mat}'s that operate only with the constituent blocks $M$, $C$ and $K$. The explicit case requires more memory but gives more flexibility, e.g., for choosing a preconditioner.

As a complete example of how to solve a quadratic eigenproblem via linearization, consider the following command line:
\begin{Verbatim}[fontsize=\small]
	$ ./program -qep_type linear -qep_hermitian -qep_linear_cform 2
                    -qep_linear_explicitmatrix -qep_eps_type krylovschur
                    -qep_st_ksp_type gmres -qep_st_pc_type bjacobi
\end{Verbatim}
The S2 linearization (Eq.\ \ref{eq:s2}) will be used, with explicit matrix storage and a preconditioned iterative method for solving linear systems with matrix $B$.

%---------------------------------------------------
\section{\label{sec:qst}Spectral Transformation}

For computing eigenvalues in the interior of the spectrum (closest to a target $\tau$), it is necessary to use a spectral transformation. Currently, only shift-and-invert is supported in \ident{QEP} solvers, and the way to handle it is via an \ident{ST} object as in the case of linear eigensolvers. Every \ident{QEP} object has an \ident{ST} object internally. Note that it would also be possible to select a spectral transformation in the \ident{ST} contained in the \ident{EPS} solver for the \texttt{QEPLINEAR} case. However, we do not recommend this approach.

Given the quadratic eigenproblem in Eq.\ \ref{eq:eigquad}, it is possible to define the transformed problem
\begin{equation}
(\theta^2M_\sigma+\theta C_\sigma+K_\sigma)x=0,\label{eq:sinvquad}
\end{equation}
where the coefficient matrices are
\begin{eqnarray}
M_\sigma&\!\!=\!\!&\sigma^2 M+\sigma C+K,\\
C_\sigma&\!\!=\!\!&C+2\sigma M,\\
K_\sigma&\!\!=\!\!&M,
\end{eqnarray}
and the relation between the eigenvalue of the original eigenproblem, $\lambda$, and the transformed one, $\theta$, is $\theta=(\lambda-\sigma)^{-1}$ as in the case of the linear eigenvalue problem. See chapter \ref{cap:st} for additional details.

A command line example would be:
	\begin{Verbatim}[fontsize=\small]
	$ ./ex16 -qep_nev 12 -qep_type qarnoldi -qep_target 0 -st_type sinvert
	\end{Verbatim}
The example computes 12 eigenpairs closest to the origin with shift-and-invert Q-Arnoldi.

%---------------------------------------------------
\section{Retrieving the Solution}

After the call to \ident{QEPSolve} has finished, the computed results are stored internally. The procedure for retrieving the computed solution is exactly the same as in the case of \ident{EPS}. The user has to call \ident{QEPGetConverged} first, to obtain the number of converged solutions, then call \ident{QEPGetEigenpair} repeatedly within a loop, once per each eigenvalue-eigenvector pair. The same considerations relative to complex eigenvalues apply, see \S\ref{sec:retrsol} for additional details.

\paragraph{Reliability of the Computed Solution.}

As in the case of linear problems, the function
	\findex{QEPComputeRelativeError}
	\begin{Verbatim}[fontsize=\small]
	QEPComputeRelativeError(QEP qep,PetscInt j,PetscReal *error);
	\end{Verbatim}
is available to assess the accuracy of the computed solutions. This error is based on the computation of the 2-norm of the residual vector, defined as
\begin{equation}
r=(\tilde{\lambda}^2M+\tilde{\lambda} C+K)\tilde{x},\label{eq:resquad}
\end{equation}
where $\tilde{\lambda}$ and $\tilde{x}$ represent any of the \texttt{nconv} computed eigenpairs delivered by \ident{QEPGetEigenpair}.

When solving the quadratic problem via linearization, an accurate solution of the generalized eigenproblem does not necessarily imply a similar level of accuracy for the quadratic problem. \cite{Tisseur:2000:BEC} shows that in the case of the N1 linearization (Eq.\ \ref{eq:n1}), a small backward error in the generalized eigenproblem guarantees a small backward error in the quadratic eigenproblem. However, this holds only if $M$, $C$ and $K$ have a similar norm.

When the norm of $M$, $C$ and $K$ vary widely, \cite{Tisseur:2000:BEC} recommends to solve the scaled problem, defined as 
\begin{equation}
(\mu^2M_\alpha+\mu C_\alpha+K)x=0,\label{eq:scaled}
\end{equation}
with $\mu=\lambda/\alpha$, $M_\alpha=\alpha^2M$ and $C_\alpha=\alpha C$, where $\alpha$ is a scaling factor. Ideally, $\alpha$ should be chosen in such a way that the norms of $M_\alpha$, $C_\alpha$ and $K$ have similar magnitude. In \slepc, this is done automatically and the user can call
	\findex{QEPSetScaleFactor}
	\begin{Verbatim}[fontsize=\small]
	QEPSetScaleFactor(QEP qep,PetscReal alpha);
	\end{Verbatim}
to specify the scaling factor. If not specified, the value $\alpha=\sqrt{\frac{\|K\|_\infty}{\|M\|_\infty}}$ will be used.

\paragraph{Controlling and Monitoring Convergence.}

As in the case of \ident{EPS}, in \ident{QEP} the number of iterations carried out by the solver can be determined with \ident{QEPGetIterationNumber}, and the tolerance and maximum number of iterations can be set with \ident{QEPSetTolerances}. Also, convergence can be monitored with command-line keys \Verb!-qep_monitor!, \Verb!-qep_monitor_all!, \Verb!-qep_monitor_conv!, \Verb!-qep_monitor_lg!, or \Verb!-qep_monitor_lg_all!. See \S\ref{sec:monitor} for additional details.

