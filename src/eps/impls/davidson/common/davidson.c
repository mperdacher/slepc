/*
   Skeleton of Davidson solver. Actual solvers are GD and JD.

   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   SLEPc - Scalable Library for Eigenvalue Problem Computations
   Copyright (c) 2002-2013, Universitat Politecnica de Valencia, Spain

   This file is part of SLEPc.

   SLEPc is free software: you can redistribute it and/or modify it under  the
   terms of version 3 of the GNU Lesser General Public License as published by
   the Free Software Foundation.

   SLEPc  is  distributed in the hope that it will be useful, but WITHOUT  ANY
   WARRANTY;  without even the implied warranty of MERCHANTABILITY or  FITNESS
   FOR  A  PARTICULAR PURPOSE. See the GNU Lesser General Public  License  for
   more details.

   You  should have received a copy of the GNU Lesser General  Public  License
   along with SLEPc. If not, see <http://www.gnu.org/licenses/>.
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*/

#include "davidson.h"

PetscErrorCode EPSView_XD(EPS eps,PetscViewer viewer);

typedef struct {
  /**** Solver options ****/
  PetscInt blocksize,     /* block size */
    initialsize,          /* initial size of V */
    minv,                 /* size of V after restarting */
    plusk;                /* keep plusk eigenvectors from the last iteration */
  EPSOrthType ipB;        /* true if B-ortho is used */
  PetscInt   method;      /* method for improving the approximate solution */
  PetscReal  fix;         /* the fix parameter */
  PetscBool  krylovstart; /* true if the starting subspace is a Krylov basis */
  PetscBool  dynamic;     /* true if dynamic stopping criterion is used */
  PetscInt   cX_in_proj,  /* converged vectors in the projected problem */
    cX_in_impr;           /* converged vectors in the projector */
  Method_t   scheme;      /* method employed: GD, JD or GD2 */

  /**** Solver data ****/
  dvdDashboard ddb;

  /**** Things to destroy ****/
  PetscScalar *wS;
  Vec         *wV;
  PetscInt    size_wV;
} EPS_DAVIDSON;

#undef __FUNCT__
#define __FUNCT__ "EPSCreate_XD"
PetscErrorCode EPSCreate_XD(EPS eps)
{
  PetscErrorCode ierr;
  EPS_DAVIDSON   *data;

  PetscFunctionBegin;
  eps->st->ops->getbilinearform  = STGetBilinearForm_Default;
  eps->ops->solve                = EPSSolve_XD;
  eps->ops->setup                = EPSSetUp_XD;
  eps->ops->reset                = EPSReset_XD;
  eps->ops->backtransform        = EPSBackTransform_Default;
  eps->ops->computevectors       = EPSComputeVectors_XD;
  eps->ops->view                 = EPSView_XD;

  ierr = PetscNewLog(eps,EPS_DAVIDSON,&data);CHKERRQ(ierr);
  eps->data = data;
  data->wS = NULL;
  data->wV = NULL;
  data->size_wV = 0;
  ierr = PetscMemzero(&data->ddb,sizeof(dvdDashboard));CHKERRQ(ierr);

  /* Set default values */
  ierr = EPSXDSetKrylovStart_XD(eps,PETSC_FALSE);CHKERRQ(ierr);
  ierr = EPSXDSetBlockSize_XD(eps,1);CHKERRQ(ierr);
  ierr = EPSXDSetRestart_XD(eps,6,0);CHKERRQ(ierr);
  ierr = EPSXDSetInitialSize_XD(eps,5);CHKERRQ(ierr);
  ierr = EPSJDSetFix_JD(eps,0.01);CHKERRQ(ierr);
  ierr = EPSXDSetBOrth_XD(eps,EPS_ORTH_B);CHKERRQ(ierr);
  ierr = EPSJDSetConstCorrectionTol_JD(eps,PETSC_TRUE);CHKERRQ(ierr);
  ierr = EPSXDSetWindowSizes_XD(eps,0,0);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EPSSetUp_XD"
PetscErrorCode EPSSetUp_XD(EPS eps)
{
  PetscErrorCode ierr;
  EPS_DAVIDSON   *data = (EPS_DAVIDSON*)eps->data;
  dvdDashboard   *dvd = &data->ddb;
  dvdBlackboard  b;
  PetscInt       nvecs,nscalars,min_size_V,plusk,bs,initv,i,cX_in_proj,cX_in_impr,nmat;
  Mat            A,B;
  KSP            ksp;
  PetscBool      t,ipB,ispositive,dynamic;
  HarmType_t     harm;
  InitType_t     init;
  PetscReal      fix;
  PetscScalar    target;

  PetscFunctionBegin;
  /* Setup EPS options and get the problem specification */
  ierr = EPSXDGetBlockSize_XD(eps,&bs);CHKERRQ(ierr);
  if (bs <= 0) bs = 1;
  if (eps->ncv) {
    if (eps->ncv<eps->nev) SETERRQ(PetscObjectComm((PetscObject)eps),PETSC_ERR_SUP,"The value of ncv must be at least nev");
  } else if (eps->mpd) eps->ncv = eps->mpd + eps->nev + bs;
  else if (eps->nev<500) eps->ncv = PetscMin(eps->n-bs,PetscMax(2*eps->nev,eps->nev+15))+bs;
  else eps->ncv = PetscMin(eps->n-bs,eps->nev+500)+bs;
  if (!eps->mpd) eps->mpd = eps->ncv;
  if (eps->mpd > eps->ncv) SETERRQ(PetscObjectComm((PetscObject)eps),PETSC_ERR_SUP,"The mpd has to be less or equal than ncv");
  if (eps->mpd < 2) SETERRQ(PetscObjectComm((PetscObject)eps),PETSC_ERR_SUP,"The mpd has to be greater than 2");
  if (!eps->max_it) eps->max_it = PetscMax(100*eps->ncv,2*eps->n);
  if (!eps->which) eps->which = EPS_LARGEST_MAGNITUDE;
  if (eps->ishermitian && (eps->which==EPS_LARGEST_IMAGINARY || eps->which==EPS_SMALLEST_IMAGINARY)) SETERRQ(PetscObjectComm((PetscObject)eps),PETSC_ERR_SUP,"Wrong value of eps->which");
  if (!(eps->nev + bs <= eps->ncv)) SETERRQ(PetscObjectComm((PetscObject)eps),PETSC_ERR_SUP,"The ncv has to be greater than nev plus blocksize");

  ierr = EPSXDGetRestart_XD(eps,&min_size_V,&plusk);CHKERRQ(ierr);
  if (!min_size_V) min_size_V = PetscMin(PetscMax(bs,5),eps->mpd/2);
  if (!(min_size_V+bs <= eps->mpd)) SETERRQ(PetscObjectComm((PetscObject)eps),PETSC_ERR_SUP,"The value of minv must be less than mpd minus blocksize");
  ierr = EPSXDGetInitialSize_XD(eps,&initv);CHKERRQ(ierr);
  if (eps->mpd < initv) SETERRQ(PetscObjectComm((PetscObject)eps),PETSC_ERR_SUP,"The initv has to be less or equal than mpd");

  /* Davidson solvers do not support left eigenvectors */
  if (eps->leftvecs) SETERRQ(PetscObjectComm((PetscObject)eps),PETSC_ERR_SUP,"Left vectors not supported in this solver");

  /* Set STPrecond as the default ST */
  if (!((PetscObject)eps->st)->type_name) {
    ierr = STSetType(eps->st,STPRECOND);CHKERRQ(ierr);
  }
  ierr = STPrecondSetKSPHasMat(eps->st,PETSC_FALSE);CHKERRQ(ierr);

  /* Change the default sigma to inf if necessary */
  if (eps->which == EPS_LARGEST_MAGNITUDE || eps->which == EPS_LARGEST_REAL ||
      eps->which == EPS_LARGEST_IMAGINARY) {
    ierr = STSetDefaultShift(eps->st,PETSC_MAX_REAL);CHKERRQ(ierr);
  }

  /* Davidson solvers only support STPRECOND */
  ierr = STSetUp(eps->st);CHKERRQ(ierr);
  ierr = PetscObjectTypeCompare((PetscObject)eps->st,STPRECOND,&t);CHKERRQ(ierr);
  if (!t) SETERRQ1(PetscObjectComm((PetscObject)eps),PETSC_ERR_SUP,"%s only works with precond spectral transformation",
    ((PetscObject)eps)->type_name);

  /* Setup problem specification in dvd */
  ierr = STGetNumMatrices(eps->st,&nmat);CHKERRQ(ierr);
  ierr = STGetOperators(eps->st,0,&A);CHKERRQ(ierr);
  if (nmat>1) { ierr = STGetOperators(eps->st,1,&B);CHKERRQ(ierr); }
  ierr = EPSReset_XD(eps);CHKERRQ(ierr);
  ierr = PetscMemzero(dvd,sizeof(dvdDashboard));CHKERRQ(ierr);
  dvd->A = A; dvd->B = eps->isgeneralized? B : NULL;
  ispositive = eps->ispositive;
  dvd->sA = DVD_MAT_IMPLICIT |
            (eps->ishermitian? DVD_MAT_HERMITIAN : 0) |
            ((ispositive && !eps->isgeneralized) ? DVD_MAT_POS_DEF : 0);
  /* Asume -eps_hermitian means hermitian-definite in generalized problems */
  if (!ispositive && !eps->isgeneralized && eps->ishermitian) ispositive = PETSC_TRUE;
  if (!eps->isgeneralized) dvd->sB = DVD_MAT_IMPLICIT | DVD_MAT_HERMITIAN | DVD_MAT_IDENTITY | DVD_MAT_UNITARY | DVD_MAT_POS_DEF;
  else dvd->sB = DVD_MAT_IMPLICIT | (eps->ishermitian? DVD_MAT_HERMITIAN : 0) | (ispositive? DVD_MAT_POS_DEF : 0);
  ipB = (dvd->B && data->ipB != EPS_ORTH_I && DVD_IS(dvd->sB,DVD_MAT_HERMITIAN))?PETSC_TRUE:PETSC_FALSE;
  if (data->ipB != EPS_ORTH_I && !ipB) data->ipB = EPS_ORTH_I;
  dvd->correctXnorm = ipB;
  dvd->sEP = ((!eps->isgeneralized || (eps->isgeneralized && ipB))? DVD_EP_STD : 0) |
             (ispositive? DVD_EP_HERMITIAN : 0) |
             ((eps->problem_type == EPS_GHIEP && ipB) ? DVD_EP_INDEFINITE : 0);
  dvd->nev = eps->nev;
  dvd->which = eps->which;
  dvd->withTarget = PETSC_TRUE;
  switch (eps->which) {
    case EPS_TARGET_MAGNITUDE:
    case EPS_TARGET_IMAGINARY:
      dvd->target[0] = target = eps->target; dvd->target[1] = 1.0;
      break;
    case EPS_TARGET_REAL:
      dvd->target[0] = PetscRealPart(target = eps->target); dvd->target[1] = 1.0;
      break;
    case EPS_LARGEST_REAL:
    case EPS_LARGEST_MAGNITUDE:
    case EPS_LARGEST_IMAGINARY: /* TODO: think about this case */
      dvd->target[0] = 1.0; dvd->target[1] = target = 0.0;
      break;
    case EPS_SMALLEST_MAGNITUDE:
    case EPS_SMALLEST_REAL:
    case EPS_SMALLEST_IMAGINARY: /* TODO: think about this case */
      dvd->target[0] = target = 0.0; dvd->target[1] = 1.0;
      break;
    case EPS_WHICH_USER:
      ierr = STGetShift(eps->st,&target);CHKERRQ(ierr);
      dvd->target[0] = target; dvd->target[1] = 1.0;
      break;
    case EPS_ALL:
      SETERRQ(PetscObjectComm((PetscObject)eps),PETSC_ERR_SUP,"Unsupported option: which == EPS_ALL");
      break;
    default:
      SETERRQ(PetscObjectComm((PetscObject)eps),PETSC_ERR_SUP,"Unsupported value of option 'which'");
  }
  dvd->tol = eps->tol==PETSC_DEFAULT?SLEPC_DEFAULT_TOL:eps->tol;
  dvd->eps = eps;

  /* Setup the extraction technique */
  if (!eps->extraction) {
    if (ipB || ispositive) eps->extraction = EPS_RITZ;
    else {
      switch (eps->which) {
        case EPS_TARGET_REAL:
        case EPS_TARGET_MAGNITUDE:
        case EPS_TARGET_IMAGINARY:
        case EPS_SMALLEST_MAGNITUDE:
        case EPS_SMALLEST_REAL:
        case EPS_SMALLEST_IMAGINARY:
          eps->extraction = EPS_HARMONIC;
          break;
        case EPS_LARGEST_REAL:
        case EPS_LARGEST_MAGNITUDE:
        case EPS_LARGEST_IMAGINARY:
          eps->extraction = EPS_HARMONIC_LARGEST;
          break;
        default:
          eps->extraction = EPS_RITZ;
      }
    }
  }
  switch (eps->extraction) {
    case EPS_RITZ:              harm = DVD_HARM_NONE; break;
    case EPS_HARMONIC:          harm = DVD_HARM_RR; break;
    case EPS_HARMONIC_RELATIVE: harm = DVD_HARM_RRR; break;
    case EPS_HARMONIC_RIGHT:    harm = DVD_HARM_REIGS; break;
    case EPS_HARMONIC_LARGEST:  harm = DVD_HARM_LEIGS; break;
    default: SETERRQ(PetscObjectComm((PetscObject)eps),PETSC_ERR_SUP,"Unsupported extraction type");
  }

  /* Setup the type of starting subspace */
  ierr = EPSXDGetKrylovStart_XD(eps,&t);CHKERRQ(ierr);
  init = (!t)? DVD_INITV_CLASSIC : DVD_INITV_KRYLOV;

  /* Setup the presence of converged vectors in the projected problem and in the projector */
  ierr = EPSXDGetWindowSizes_XD(eps,&cX_in_impr,&cX_in_proj);CHKERRQ(ierr);
  if (min_size_V <= cX_in_proj) SETERRQ(PetscObjectComm((PetscObject)eps),PETSC_ERR_SUP,"minv has to be greater than qwindow");
  if (bs > 1 && cX_in_impr > 0) SETERRQ(PetscObjectComm((PetscObject)eps),PETSC_ERR_SUP,"Unsupported option: pwindow > 0 and bs > 1");

  /* Setup IP */
  if (ipB && dvd->B) {
    ierr = IPSetMatrix(eps->ip,dvd->B);CHKERRQ(ierr);
  } else {
    ierr = IPSetMatrix(eps->ip,NULL);CHKERRQ(ierr);
  }

  /* Get the fix parameter */
  ierr = EPSXDGetFix_XD(eps,&fix);CHKERRQ(ierr);

  /* Get whether the stopping criterion is used */
  ierr = EPSJDGetConstCorrectionTol_JD(eps,&dynamic);CHKERRQ(ierr);

  /* Orthonormalize the deflation space */
  ierr = dvd_orthV(eps->ip,NULL,0,NULL,0,eps->defl,0,PetscAbs(eps->nds),NULL,eps->rand);CHKERRQ(ierr);

  /* Preconfigure dvd */
  ierr = STGetKSP(eps->st,&ksp);CHKERRQ(ierr);
  ierr = dvd_schm_basic_preconf(dvd,&b,eps->mpd,min_size_V,bs,
                                initv,
                                PetscAbs(eps->nini),
                                plusk,harm,
                                ksp,init,eps->trackall,
                                data->ipB,cX_in_proj,cX_in_impr,
                                data->scheme);CHKERRQ(ierr);

  /* Allocate memory */
  nvecs = b.max_size_auxV + b.own_vecs;
  nscalars = b.own_scalars + b.max_size_auxS;
  ierr = PetscMalloc(nscalars*sizeof(PetscScalar),&data->wS);CHKERRQ(ierr);
  ierr = PetscLogObjectMemory((PetscObject)eps,nscalars*sizeof(PetscScalar));CHKERRQ(ierr);
  ierr = VecDuplicateVecs(eps->t,nvecs,&data->wV);CHKERRQ(ierr);
  ierr = PetscLogObjectParents(eps,nvecs,data->wV);CHKERRQ(ierr);
  data->size_wV = nvecs;
  b.free_vecs = data->wV;
  b.free_scalars = data->wS;
  dvd->auxV = data->wV + b.own_vecs;
  dvd->auxS = b.free_scalars + b.own_scalars;
  dvd->size_auxV = b.max_size_auxV;
  dvd->size_auxS = b.max_size_auxS;

  eps->errest_left = NULL;
  ierr = PetscMalloc(eps->ncv*sizeof(PetscInt),&eps->perm);CHKERRQ(ierr);
  ierr = PetscLogObjectMemory((PetscObject)eps,eps->ncv*sizeof(PetscInt));CHKERRQ(ierr);
  for (i=0;i<eps->ncv;i++) eps->perm[i] = i;

  /* Configure dvd for a basic GD */
  ierr = dvd_schm_basic_conf(dvd,&b,eps->mpd,min_size_V,bs,
                             initv,
                             PetscAbs(eps->nini),plusk,
                             eps->ip,harm,dvd->withTarget,
                             target,ksp,
                             fix,init,eps->trackall,
                             data->ipB,cX_in_proj,cX_in_impr,dynamic,
                             data->scheme);CHKERRQ(ierr);

  /* Associate the eigenvalues to the EPS */
  eps->eigr = dvd->real_eigr;
  eps->eigi = dvd->real_eigi;
  eps->errest = dvd->real_errest;
  eps->V = dvd->real_V;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EPSSolve_XD"
PetscErrorCode EPSSolve_XD(EPS eps)
{
  EPS_DAVIDSON   *data = (EPS_DAVIDSON*)eps->data;
  dvdDashboard   *d = &data->ddb;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  /* Call the starting routines */
  DVD_FL_CALL(d->startList,d);

  for (eps->its=0;eps->its<eps->max_it;eps->its++) {
    /* Initialize V, if it is needed */
    if (d->size_V == 0) { ierr = d->initV(d);CHKERRQ(ierr); }

    /* Find the best approximated eigenpairs in V, X */
    ierr = d->calcPairs(d);CHKERRQ(ierr);

    /* Test for convergence */
    if (eps->nconv >= eps->nev) break;

    /* Expand the subspace */
    ierr = d->updateV(d);CHKERRQ(ierr);

    /* Monitor progress */
    eps->nconv = d->nconv;
    ierr = EPSMonitor(eps,eps->its+1,eps->nconv,eps->eigr,eps->eigi,eps->errest,d->size_V+d->size_cX);CHKERRQ(ierr);
  }

  /* Call the ending routines */
  DVD_FL_CALL(d->endList,d);

  if (eps->nconv >= eps->nev) eps->reason = EPS_CONVERGED_TOL;
  else eps->reason = EPS_DIVERGED_ITS;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EPSReset_XD"
PetscErrorCode EPSReset_XD(EPS eps)
{
  EPS_DAVIDSON   *data = (EPS_DAVIDSON*)eps->data;
  dvdDashboard   *dvd = &data->ddb;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  /* Call step destructors and destroys the list */
  DVD_FL_CALL(dvd->destroyList,dvd);
  DVD_FL_DEL(dvd->destroyList);
  DVD_FL_DEL(dvd->startList);
  DVD_FL_DEL(dvd->endList);

  if (data->size_wV > 0) {
    ierr = VecDestroyVecs(data->size_wV,&data->wV);CHKERRQ(ierr);
  }
  ierr = PetscFree(data->wS);CHKERRQ(ierr);
  ierr = PetscFree(eps->perm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EPSView_XD"
PetscErrorCode EPSView_XD(EPS eps,PetscViewer viewer)
{
  PetscErrorCode ierr;
  PetscBool      isascii,opb;
  PetscInt       opi,opi0;
  Method_t       meth;
  EPSOrthType    borth;

  PetscFunctionBegin;
  ierr = PetscObjectTypeCompare((PetscObject)viewer,PETSCVIEWERASCII,&isascii);CHKERRQ(ierr);
  if (isascii) {
    ierr = EPSXDGetMethod_XD(eps,&meth);CHKERRQ(ierr);
    if (meth==DVD_METH_GD2) {
      ierr = PetscViewerASCIIPrintf(viewer,"  Davidson: using double expansion variant (GD2)\n");CHKERRQ(ierr);
    }
    ierr = EPSXDGetBOrth_XD(eps,&borth);CHKERRQ(ierr);
    switch (borth) {
    case EPS_ORTH_I:
      ierr = PetscViewerASCIIPrintf(viewer,"  Davidson: search subspace is orthogonalized\n");CHKERRQ(ierr);
      break;
    case EPS_ORTH_B:
      ierr = PetscViewerASCIIPrintf(viewer,"  Davidson: search subspace is B-orthogonalized\n");CHKERRQ(ierr);
      break;
    case EPS_ORTH_BOPT:
      ierr = PetscViewerASCIIPrintf(viewer,"  Davidson: search subspace is B-orthogonalized with an optimized method\n");CHKERRQ(ierr);
      break;
    }
    ierr = EPSXDGetBlockSize_XD(eps,&opi);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(viewer,"  Davidson: block size=%D\n",opi);CHKERRQ(ierr);
    ierr = EPSXDGetKrylovStart_XD(eps,&opb);CHKERRQ(ierr);
    if (!opb) {
      ierr = PetscViewerASCIIPrintf(viewer,"  Davidson: type of the initial subspace: non-Krylov\n");CHKERRQ(ierr);
    } else {
      ierr = PetscViewerASCIIPrintf(viewer,"  Davidson: type of the initial subspace: Krylov\n");CHKERRQ(ierr);
    }
    ierr = EPSXDGetRestart_XD(eps,&opi,&opi0);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(viewer,"  Davidson: size of the subspace after restarting: %D\n",opi);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(viewer,"  Davidson: number of vectors after restarting from the previous iteration: %D\n",opi0);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EPSXDSetKrylovStart_XD"
PetscErrorCode EPSXDSetKrylovStart_XD(EPS eps,PetscBool krylovstart)
{
  EPS_DAVIDSON *data = (EPS_DAVIDSON*)eps->data;

  PetscFunctionBegin;
  data->krylovstart = krylovstart;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EPSXDGetKrylovStart_XD"
PetscErrorCode EPSXDGetKrylovStart_XD(EPS eps,PetscBool *krylovstart)
{
  EPS_DAVIDSON *data = (EPS_DAVIDSON*)eps->data;

  PetscFunctionBegin;
  *krylovstart = data->krylovstart;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EPSXDSetBlockSize_XD"
PetscErrorCode EPSXDSetBlockSize_XD(EPS eps,PetscInt blocksize)
{
  EPS_DAVIDSON *data = (EPS_DAVIDSON*)eps->data;

  PetscFunctionBegin;
  if (blocksize == PETSC_DEFAULT || blocksize == PETSC_DECIDE) blocksize = 1;
  if (blocksize <= 0) SETERRQ(PetscObjectComm((PetscObject)eps),PETSC_ERR_ARG_OUTOFRANGE,"Invalid blocksize value");
  data->blocksize = blocksize;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EPSXDGetBlockSize_XD"
PetscErrorCode EPSXDGetBlockSize_XD(EPS eps,PetscInt *blocksize)
{
  EPS_DAVIDSON *data = (EPS_DAVIDSON*)eps->data;

  PetscFunctionBegin;
  *blocksize = data->blocksize;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EPSXDSetRestart_XD"
PetscErrorCode EPSXDSetRestart_XD(EPS eps,PetscInt minv,PetscInt plusk)
{
  EPS_DAVIDSON *data = (EPS_DAVIDSON*)eps->data;

  PetscFunctionBegin;
  if (minv == PETSC_DEFAULT || minv == PETSC_DECIDE) minv = 5;
  if (minv <= 0) SETERRQ(PetscObjectComm((PetscObject)eps),PETSC_ERR_ARG_OUTOFRANGE,"Invalid minv value");
  if (plusk == PETSC_DEFAULT || plusk == PETSC_DECIDE) plusk = 5;
  if (plusk < 0) SETERRQ(PetscObjectComm((PetscObject)eps),PETSC_ERR_ARG_OUTOFRANGE,"Invalid plusk value");
  data->minv = minv;
  data->plusk = plusk;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EPSXDGetRestart_XD"
PetscErrorCode EPSXDGetRestart_XD(EPS eps,PetscInt *minv,PetscInt *plusk)
{
  EPS_DAVIDSON *data = (EPS_DAVIDSON*)eps->data;

  PetscFunctionBegin;
  if (minv) *minv = data->minv;
  if (plusk) *plusk = data->plusk;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EPSXDGetInitialSize_XD"
PetscErrorCode EPSXDGetInitialSize_XD(EPS eps,PetscInt *initialsize)
{
  EPS_DAVIDSON *data = (EPS_DAVIDSON*)eps->data;

  PetscFunctionBegin;
  *initialsize = data->initialsize;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EPSXDSetInitialSize_XD"
PetscErrorCode EPSXDSetInitialSize_XD(EPS eps,PetscInt initialsize)
{
  EPS_DAVIDSON *data = (EPS_DAVIDSON*)eps->data;

  PetscFunctionBegin;
  if (initialsize == PETSC_DEFAULT || initialsize == PETSC_DECIDE) initialsize = 5;
  if (initialsize <= 0) SETERRQ(PetscObjectComm((PetscObject)eps),PETSC_ERR_ARG_OUTOFRANGE,"Invalid initial size value");
  data->initialsize = initialsize;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EPSXDGetFix_XD"
PetscErrorCode EPSXDGetFix_XD(EPS eps,PetscReal *fix)
{
  EPS_DAVIDSON *data = (EPS_DAVIDSON*)eps->data;

  PetscFunctionBegin;
  *fix = data->fix;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EPSJDSetFix_JD"
PetscErrorCode EPSJDSetFix_JD(EPS eps,PetscReal fix)
{
  EPS_DAVIDSON *data = (EPS_DAVIDSON*)eps->data;

  PetscFunctionBegin;
  if (fix == PETSC_DEFAULT || fix == PETSC_DECIDE) fix = 0.01;
  if (fix < 0.0) SETERRQ(PetscObjectComm((PetscObject)eps),PETSC_ERR_ARG_OUTOFRANGE,"Invalid fix value");
  data->fix = fix;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EPSXDSetBOrth_XD"
PetscErrorCode EPSXDSetBOrth_XD(EPS eps,EPSOrthType borth)
{
  EPS_DAVIDSON *data = (EPS_DAVIDSON*)eps->data;

  PetscFunctionBegin;
  data->ipB = borth;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EPSXDGetBOrth_XD"
PetscErrorCode EPSXDGetBOrth_XD(EPS eps,EPSOrthType *borth)
{
  EPS_DAVIDSON *data = (EPS_DAVIDSON*)eps->data;

  PetscFunctionBegin;
  *borth = data->ipB;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EPSJDSetConstCorrectionTol_JD"
PetscErrorCode EPSJDSetConstCorrectionTol_JD(EPS eps,PetscBool constant)
{
  EPS_DAVIDSON *data = (EPS_DAVIDSON*)eps->data;

  PetscFunctionBegin;
  data->dynamic = !constant?PETSC_TRUE:PETSC_FALSE;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EPSJDGetConstCorrectionTol_JD"
PetscErrorCode EPSJDGetConstCorrectionTol_JD(EPS eps,PetscBool *constant)
{
  EPS_DAVIDSON *data = (EPS_DAVIDSON*)eps->data;

  PetscFunctionBegin;
  *constant = !data->dynamic?PETSC_TRUE:PETSC_FALSE;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EPSXDSetWindowSizes_XD"
PetscErrorCode EPSXDSetWindowSizes_XD(EPS eps,PetscInt pwindow,PetscInt qwindow)
{
  EPS_DAVIDSON *data = (EPS_DAVIDSON*)eps->data;

  PetscFunctionBegin;
  if (pwindow == PETSC_DEFAULT || pwindow == PETSC_DECIDE) pwindow = 0;
  if (pwindow < 0) SETERRQ(PetscObjectComm((PetscObject)eps),PETSC_ERR_ARG_OUTOFRANGE,"Invalid pwindow value");
  if (qwindow == PETSC_DEFAULT || qwindow == PETSC_DECIDE) qwindow = 0;
  if (qwindow < 0) SETERRQ(PetscObjectComm((PetscObject)eps),PETSC_ERR_ARG_OUTOFRANGE,"Invalid qwindow value");
  data->cX_in_proj = qwindow;
  data->cX_in_impr = pwindow;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EPSXDGetWindowSizes_XD"
PetscErrorCode EPSXDGetWindowSizes_XD(EPS eps,PetscInt *pwindow,PetscInt *qwindow)
{
  EPS_DAVIDSON *data = (EPS_DAVIDSON*)eps->data;

  PetscFunctionBegin;
  if (pwindow) *pwindow = data->cX_in_impr;
  if (qwindow) *qwindow = data->cX_in_proj;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EPSXDSetMethod"
PetscErrorCode EPSXDSetMethod(EPS eps,Method_t method)
{
  EPS_DAVIDSON *data = (EPS_DAVIDSON*)eps->data;

  PetscFunctionBegin;
  data->scheme = method;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EPSXDGetMethod_XD"
PetscErrorCode EPSXDGetMethod_XD(EPS eps,Method_t *method)
{
  EPS_DAVIDSON *data = (EPS_DAVIDSON*)eps->data;

  PetscFunctionBegin;
  *method = data->scheme;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EPSComputeVectors_XD"
/*
  EPSComputeVectors_XD - Compute eigenvectors from the vectors
  provided by the eigensolver. This version is intended for solvers
  that provide Schur vectors from the QZ decompositon. Given the partial
  Schur decomposition OP*V=V*T, the following steps are performed:
      1) compute eigenvectors of (S,T): S*Z=T*Z*D
      2) compute eigenvectors of OP: X=V*Z
  If left eigenvectors are required then also do Z'*T=D*Z', Y=W*Z
 */
PetscErrorCode EPSComputeVectors_XD(EPS eps)
{
  PetscErrorCode ierr;
  EPS_DAVIDSON   *data = (EPS_DAVIDSON*)eps->data;
  dvdDashboard   *d = &data->ddb;
  PetscScalar    *pX,*cS,*cT;
  PetscInt       ld;

  PetscFunctionBegin;
  if (d->cS) {
    /* Compute the eigenvectors associated to (cS, cT) */
    ierr = DSSetDimensions(d->conv_ps,d->size_cS,0,0,0);CHKERRQ(ierr);
    ierr = DSGetLeadingDimension(d->conv_ps,&ld);CHKERRQ(ierr);
    ierr = DSGetArray(d->conv_ps,DS_MAT_A,&cS);CHKERRQ(ierr);
    ierr = SlepcDenseCopyTriang(cS,0,ld,d->cS,0,d->ldcS,d->size_cS,d->size_cS);CHKERRQ(ierr);
    ierr = DSRestoreArray(d->conv_ps,DS_MAT_A,&cS);CHKERRQ(ierr);
    if (d->cT) {
      ierr = DSGetArray(d->conv_ps,DS_MAT_B,&cT);CHKERRQ(ierr);
      ierr = SlepcDenseCopyTriang(cT,0,ld,d->cT,0,d->ldcT,d->size_cS,d->size_cS);CHKERRQ(ierr);
      ierr = DSRestoreArray(d->conv_ps,DS_MAT_B,&cT);CHKERRQ(ierr);
    }
    ierr = DSSetState(d->conv_ps,DS_STATE_RAW);CHKERRQ(ierr);
    ierr = DSSolve(d->conv_ps,eps->eigr,eps->eigi);CHKERRQ(ierr);
    ierr = DSVectors(d->conv_ps,DS_MAT_X,NULL,NULL);CHKERRQ(ierr);
    ierr = DSNormalize(d->conv_ps,DS_MAT_X,-1);CHKERRQ(ierr);

    /* V <- cX * pX */
    ierr = DSGetArray(d->conv_ps,DS_MAT_X,&pX);CHKERRQ(ierr);
    ierr = SlepcUpdateVectorsZ(eps->V,0.0,1.0,d->cX,d->size_cX,pX,ld,d->nconv,d->nconv);CHKERRQ(ierr);
    ierr = DSRestoreArray(d->conv_ps,DS_MAT_X,&pX);CHKERRQ(ierr);
  }

  eps->evecsavailable = PETSC_TRUE;
  PetscFunctionReturn(0);
}
